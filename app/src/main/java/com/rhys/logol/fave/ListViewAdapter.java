package com.rhys.logol.fave;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

class ListViewAdapter extends BaseAdapter {

    // Declare Variables
    private Context context;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    private List<Photos> photosList = null;

    ListViewAdapter(Context context, List<Photos> photosList) {
        this.context = context;
        this.photosList = photosList;
        inflater = LayoutInflater.from(context);
        ArrayList<Photos> arraylist = new ArrayList<>();
        arraylist.addAll(photosList);
        imageLoader = new ImageLoader(context);
    }

    private class ViewHolder {
        ImageView flag;
    }

    @Override
    public int getCount() {
        return photosList.size();
    }

    @Override
    public Object getItem(int position) {
        return photosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);
            // Locate the ImageView in listview_item.xml
            holder.flag = (ImageView) view.findViewById(R.id.flag);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into ImageView
        imageLoader.DisplayImage(photosList.get(position).getImg(),holder.flag);

        // TODO: Listen for like clicks here
        return view;
    }

}