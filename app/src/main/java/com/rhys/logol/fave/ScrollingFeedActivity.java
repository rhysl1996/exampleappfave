package com.rhys.logol.fave;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ScrollingFeedActivity extends AppCompatActivity {
    ListView listview;
    private List<Photos> photos = null;
    ListViewAdapter adapter;
    private ImageView imageView;
    private ImageView picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_main);

        imageView = (ImageView) findViewById(R.id.flag);
        picture = (ImageView) findViewById(R.id.picture);
        listview = (ListView) findViewById(R.id.listview);

        // Execute RemoteDataTask AsyncTask
        new RemoteDataTask().execute();
    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            photos = new ArrayList<>();

            ParseUser currentUser = ParseUser.getCurrentUser();
            ParseRelation<ParseObject> relation = currentUser.getRelation("kk");
            ParseQuery<ParseObject> imageQuery = relation.getQuery();

            imageQuery.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        //not found
                    } else {
                        for(ParseObject object : objects) {
                            Log.d("test", "Retrieved the object.");
                            ParseFile fileObject = (ParseFile) object.get("ImageFile");
                            loadImages(fileObject,picture);
                        }
                    }
                }
            });
            return null;
        }

        private void loadImages(ParseFile thumbnail, final ImageView img) {
            if (thumbnail != null) {
                thumbnail.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                            img.setImageBitmap(bmp);
                        }
                    }
                });
            } else {
                img.setImageResource(R.drawable.temp_img);
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.listview);
            // Pass the results into ListViewAdapter.java
            adapter = new ListViewAdapter(ScrollingFeedActivity.this,
                    photos);
            // Binds the Adapter to the ListView
            listview.setAdapter(adapter);
        }
    }
}
