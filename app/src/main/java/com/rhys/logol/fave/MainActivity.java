package com.rhys.logol.fave;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends AppCompatActivity {

    EditText et_username;
    EditText et_password;

    Button login;
    Button register;

    ProgressDialog progressDialog;

    TextView t_username;
    TextView t_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
        t_username = (TextView) findViewById(R.id.usernameText);
        t_email = (TextView) findViewById(R.id.emailText);
        progressDialog = new ProgressDialog(MainActivity.this);

        //Initialize parse server
        getKeyHash();
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("mnZsRbyBpSM6Q2vTvnSOPFsxyqxSDq137WozgeWq")
                .clientKey("npRpQIgkPdTYpEK3I8X7mB5bltgdScpPAk4mzkL0")
                .server("https://parseapi.back4app.com/").build()
        );

        //setOnClickListener() for login button
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Please Wait");
                progressDialog.setTitle("Logging in");
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            parseLogin();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        //setOnClickListener() for register button
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Please Wait");
                progressDialog.setTitle("Registering");
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            parseRegister();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    /**
     * Checks users login information.
     * */
    void parseLogin(){
        ParseUser.logInInBackground(et_username.getText().toString(), et_password.getText().toString(), new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (parseUser != null) {
                    progressDialog.dismiss();
                    getUserDetailFromParse();
                    // Begin to load users feed of pictures
                    afterLogin();
                } else {
                    progressDialog.dismiss();
                    alertDisplayer("Login Fail", e.getMessage()+" Please re-try");
                }
            }
        });
    }

    //Creates new user
    void parseRegister(){
        ParseUser user = new ParseUser();
        user.setUsername(et_username.getText().toString());
        user.setPassword(et_password.getText().toString());
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    progressDialog.dismiss();
                    t_username.setText(ParseUser.getCurrentUser().getUsername());
                } else {
                    progressDialog.dismiss();
                    alertDisplayer("Register Fail", e.getMessage());
                }
            }
        });
    }

    /**
     * Displays login alerts
     * @param title
     * - Displayed as a larger font
     * @param message
     * - Displayed as a smaller font
     * */
    void alertDisplayer(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog ok = builder.create();
        ok.show();
    }

    /**
     * Stores hash key in log
     * */
    private void getKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("scm.dominwong4.back4appandroidtutorial", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            //something
        }
    }

    /**
     * Displays Welcome alert upon signing in.
     * */
    void getUserDetailFromParse(){
        ParseUser user = ParseUser.getCurrentUser();
        t_username.setText(user.getUsername());
        t_email.setText(user.getEmail());
        alertDisplayer("Welcome Back", "" + user.getUsername());
    }

    /**
     * Starts ScrollingFeedActivity and finishes MainActivity.
     * */
    void afterLogin(){
        Intent intent = new Intent(this, ScrollingFeedActivity.class);
        startActivity(intent);
        finish();
    }
}